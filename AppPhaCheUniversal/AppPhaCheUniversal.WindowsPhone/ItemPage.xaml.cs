﻿using AppPhaCheUniversal.Common;
using AppPhaCheUniversal.Data;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.UI;
using Windows.UI.Notifications;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Universal Hub Application project template is documented at http://go.microsoft.com/fwlink/?LinkID=391955

namespace AppPhaCheUniversal
{
    /// <summary>
    /// A page that displays details for a single item within a group.
    /// </summary>
    public sealed partial class ItemPage : Page
    {
        private readonly NavigationHelper navigationHelper;
        StatusBar statusBar;
        Item item;

        public ItemPage()
        {
            this.InitializeComponent();

            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += this.NavigationHelper_LoadState;
            this.navigationHelper.SaveState += this.NavigationHelper_SaveState;
        }
        void setWeb(string content)
        {
            string htmlScript = "<script>" +
                                          "function getDocHeight() { " +
                                          "return document.getElementById('pageWrapper').offsetHeight;}" +
                                          "function SendDataToPhoneApp() {" +
                                          "window.external.notify('' + getDocHeight());" +
                                          "}" +

                                          "</script><style>.clear {clear: both;} .titleInstruction { float: left; color: #333; font-weight: normal; font-size: 24px; padding-top: 2px; border-bottom: 4px solid #3D9BF9; font-family: \"Times New Roman\",Times,serif; }.detail-material {font-size: 0;background: url('http://www.lamsao.com/Resources/Skins/Default/Images/Details/22/material.png') no-repeat top left;width: 169px;height: 61px; margin-top: 10px;} .detail-make {font-size: 0;background: url('http://www.lamsao.com/Resources/Skins/Default/Images/Details/22/make.png') no-repeat top left;width: 169px;height: 61px; margin-top: 10px;} .detail-result {font-size: 0;background: url('http://www.lamsao.com/Resources/Skins/Default/Images/Details/22/result.png') no-repeat top left;width: 169px;height: 61px; margin-top: 10px;}</style>";
            //string c = "<p><strong>Chia sẻ cách nuôi dạy con tốt thông minh của người Nhật<strong> của một bà mẹ đã trải qua những năm tháng nuôi dạy con đầu đời một cách đúng đắn mang lại hiệu quả cao. Dạy trẻ đúng cách sẽ giúp các bé hình thành nhân cách ngay từ nhỏ, trong vấn đề nuôi dạy con này thì người Nhật làm rất tốt. Vậy người Nhật đã nuôi dạy con như thế nào trong quá trình phát triển tâm lý của trẻ. Hãy cùng mecuti.vn tham khảo những thông tin dưới đây để có thêm kinh nghiệm chăm sóc con cái nhé!</p><img style=\"width:100px;height:100px;\" src='http://eva-img.24hstatic.com/upload/4-2014/images/2014-12-26/1419563838-121113afamilymbanh5-08b9a.jpg' /><p>Nuôi dạy trẻ 1 2 3 4 5 6 tuổi đúng cách : ứng xử khi trẻ khóc các bậc cha mẹ nên làm gì ? - phần 1</p><h2>Dạy trẻ như thế nào là đúng ?</h2><p>Cho trẻ ăn hay uống quá nhiều đều không tốt, có khi còn sinh bệnh. Cái mà có thể làm cho trẻ vui không phải chỉ là ăn uống. Ngoài tác động vị giác còn có tác động thị giác và thính giác nữa. Vì thế khi con khóc, tôi gõ chuông cho con nghe, lắc những thứ phát ra âm thanh, hoặc gây chú ý cho con bằng các quả bóng bay…</p><p>Bởi vì trẻ cần phải được phát huy các năng lực tiềm tàng của mình, nên các trò chơi cũng phải đáp ứng được tiêu chí đó. Tôi dành một góc phòng của con làm nơi tập thể dục. Ở đó tôi đặt những dụng cụ phù hợp với việc rèn luyện sức khỏe, con của tôi có thể tập gậy, đánh đu,… Các trò chơi của bé đều nhằm mục đích nâng cao trí tuệ, thể lực, đạo đức, phát huy tốt nhất mọi khả năng của bản thân, không lãng phí sức lực một cách vô ích.</p><p>Nuôi dạy trẻ 1 2 3 4 5 6 tuổi đúng cách : ứng xử khi trẻ khóc các bậc cha mẹ nên làm gì ? - phần 2</p><p>Trẻ con thường thích những trò chơi bắt chước, diễn kịch. Những trò chơi kiểu này lại rất hiệu quả trong việc mở rộng hiểu biết của trẻ. Ở các rạp hát dành riêng cho trẻ em, nếu tổ chức được các kịch bản như thế sẽ rất có ích. Tôi nghĩ rằng các bộ phim cũng có thể mở ra nhiều vấn đề để bàn luận, nhưng nếu đem so sánh thì loại kịch này có tính giáo dục thiết thực hơn. Tôi thường xuyên dẫn con đi đến những chỗ như thế, và khi về 2 mẹ con sẽ cùng diễn lại, nếu không đủ người thì sẽ dùng búp bê.</p><p>Tôi và con cũng hay chơi trò chơi bịt mắt. Bé sẽ bị che mắt và đoán xem một vật nào đó là vật gì, hoặc là đi quanh phòng để xác định đồ vật. Loại trò chơi này giúp trẻ phát triển xúc giác.</p><p>Để phát triển thị giác, chúng tôi chơi trò đố “cái này có bao nhiêu”. Tôi sẽ xếp các quân cờ tướng hoặc hạt đậu trên bàn, sau đó cho bé nhìn qua 1 chút và đố xem số lượng là bao nhiêu. Với trò chơi này thì có rất nhiều cơ hội để áp dụng. Chẳng hạn khi ăn có thể đố có bao nhiêu miếng táo trên đĩa, hay khi cùng nhau đi bộ trên đường có thể đố có bao nhiêu thứ gì đó trên vỉa hè… Trò chơi này giúp trẻ trở nên nhạy bén và có trí nhớ tốt.</p><p>Khi con nhà tôi còn rất nhỏ, tôi thường dẫn con đi đến nhiều nơi, lần sau đến đó tôi để con đi trước và dẫn tôi theo. Đến khi bé được 16 tháng tuổi đã có thể dẫn mẹ đến khá nhiều địa điểm.</p><p>Nói về các trò chơi phát triển thị giác thì có rất nhiều. Lấy ví dụ tôi nghĩ về món đồ gì đó trong phòng, và bảo là nó màu đỏ. Khi đó con của tôi sẽ lần lượt tìm các đồ màu đỏ trong phòng để đoán, và chỉ được đoán trong một số lần nhất định, giả sử 3 hoặc 5. Nếu không đoán trúng thì sẽ thua cuộc.</p><p>Lại nói về trò chơi với bảng cửu chương. Tôi viết các phép tính vào các mảnh bìa, ví dụ 5×7, 8×9. Có rất nhiều các mảnh như thế, sẽ lần lượt được đưa ra. Người chơi phải ngay lập tức trả lời là 35, 72. Nếu trả lời chậm hoặc sai thì người kia sẽ trả lời thay và được lấy tấm bìa đó.</p><p>Khi lái máy bay cần phải nhớ những thao tác điều khiển máy bay. Trẻ con cũng cần phải nhớ những thao tác của bản thân mình, hay nói cách khác là biết điều khiển tay chân mình. Để làm được điều đó có 1 trò chơi gọi là “bắt chước tượng đồng”, có nguồn gốc từ người Hy Lạp. Khi chơi trò này, 1 người sẽ giữ nguyên 1 tư thế nhất định trong khi người kia sẽ đếm đến 50 hoặc 100, nếu cử động sẽ bị thua.</p><p>Ta cũng có thể dùng vải, giấy,… để tạo ra nhiều đồ chơi rèn luyện sự khéo léo của đôi tay. Các trò chơi cần vận động đầu óc cũng rất nhiều, và trẻ có thể chơi mà không biết chán. Tôi thường dùng giấy để xếp thuyền và bướm cho con, dùng vải để làm búp bê, dùng vỏ bao thuốc lá làm xe ngựa, dùng các hộp to để xây nhà và thành trì, làm cầu và tháp… Ngoài ra tôi còn dùng hạt đậu Nam Kinh làm thành hình người, dùng quả chuối làm ngựa. Những thứ này không phải chỉ nhằm để chơi mà còn có tác dụng phát huy khả năng sáng tạo của trẻ.</p><p>Lúc nhỏ con của tôi được dạy cả việc khâu vá và may quần áo cho búp bê. Năm 4 tuổi, bé lần đầu tiên tặng mẹ một món quà, đó là con búp bê đội mũ được khâu bằng rất nhiều chỉ. Tôi cũng dạy bé đan, và bé đã làm ra được khá nhiều thứ. Tuy nhiên việc khâu vá nếu kéo dài sẽ ảnh hưởng đến thần kinh, vì thế mỗi lần chỉ nên làm khoảng 15 phút. Với việc như vẽ tranh thì có thể lâu hơn, tầm 30 phút. Nhưng nhìn chung việc gì cũng không được để trẻ làm đến mức cực nhọc.</p><p>Đối với trẻ con thì đồ ăn, trò chơi, bạn chơi, nên thường xuyên thay đổi. Emerson có nói: Nếu thế giới chỉ có 2 người thì trong vòng 1 ngày giữa họ sẽ hình thành quan hệ chủ-tớ. Vì thế, tôi rất chú ý để con không chỉ chơi suốt với 1 người bạn, tránh cho giữa chúng nảy sinh kiểu quan hệ như trên. Có những người nói rằng, để cho bé trai và bé gái chơi với nhau là không tốt, nhưng tôi thì nghĩ ngược lại. Khi chơi như thế, bé gái sẽ dịu dàng nữ tính hơn, và bé trai sẽ có cơ hội để chứng tỏ sự dũng cảm, khí phách của mình. Hai bên sẽ đều nhanh chóng trở nên người lớn hơn.</p><p>Nuôi dạy trẻ 1 2 3 4 5 6 tuổi đúng cách : ứng xử khi trẻ khóc các bậc cha mẹ nên làm gì ? - phần 3</p><p>Làm vườn cũng rất tốt cho việc mở mang kiến thức và rèn luyện sức khỏe cho trẻ. Khi con vừa biết đi, tôi mua một cái mai và một cái xẻng nhỏ. Tôi dạy cho bé cách gieo hạt, trồng hoa, nhỏ cỏ, tưới cây. Tôi nhận thấy rằng những công việc này cũng góp phần bồi đắp cảm hứng và rèn luyện tính kiên nhẫn cho con trẻ.</p><p>Có nhiều bà mẹ không có hứng thú với các trò chơi của con. Trong khi họ bận bịu việc khâu và, bếp núc, con làm được bao nhiêu thứ và sung sướng đem khoe mẹ nhưng họ cũng không thèm nhìn. Như vậy trẻ sẽ buồn bực và dẫn đến phá phách, rồi lại bị đánh mắng. Nhưng đó hoàn toàn là do lỗi của người lớn.</p><p>Người làm cha mẹ phải nỗ lực để cùng chơi với con trẻ. Người mẹ mà vì bận việc nhà mà bỏ mặc con tự chơi là không hoàn thành nghĩa vụ giáo dục con. Trong thời đại tiến bộ ngày nay, làm gì cũng phải tính toán, không nhất thiết phải ôm tất cả mọi việc vào mình mà nên tận dụng sự tiến bộ của nhân loại để làm sao trong 1 ngày không lãng phí thời gian đáng lẽ ra phải dành để giáo dục con. Để trẻ tự chơi chắc chắn sẽ dẫn đến cãi cọ, khóc lóc. Người lớn phải luôn giám sát được sự chơi đùa của trẻ cả khi trong nhà lẫn bên ngoài. Về điểm này phụ nữ nước ta phải học tập các bà mẹ Nhật Bản. Vào dịp Tết, vào các tháng 3,5,7, họ đều có lễ hội trong đó người lớn làm rất nhiều thứ cho trẻ em được vui chơi. Một điểm nữa ở các bà mẹ Nhật Bản là trong phòng họ không dành riêng ra một góc chuyên xếp những đồ lặt vặt trong nhà và cấm con trẻ được động vào. Họ thường dọn dẹp sạch sẽ mọi thứ để con được vui chơi ở những chỗ rộng rãi. Họ cũng không đi giày dép trong nhà, vì thế sàn nhà luôn sạch sẽ và trẻ có thể chơi thoải mái trên đó. Nhân tiện tôi xin nói thêm ở đây về trò chơi bài của Nhật Bản. Đó là một loại trò chơi rất có tác dụng trong việc giúp trẻ lanh lợi và có trí ghi nhớ tốt. Đối với các môn học như lịch sử, địa lý, toán học, tôi đều dùng cách viết ra các tấm bìa và cùng chơi với con dưới dạng chơi bài, hiệu quả thực sự đáng ngạc nhiên.</p><p>Tính xã giao với trẻ cũng rất cần thiết. Nên khuyến khích trẻ tổ chức và tham gia các hội, tất nhiên phải đảm bảo vừa vui vừa có ích. Con của tôi hiện đang là hội viên hội tương trợ thiếu niên, chuyên gửi hoa và đồ chơi tự làm đến cho các em nhỏ bị đau ốm.</p>";
            string tempHTML = "<html><head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no\" /><meta name=\"HandheldFriendly\" content=\"true\" />" + htmlScript +
                                    "</head>" + "<body style=\"margin:0px; padding:0px; width:100%; -ms-user-select: none;color:black; background: white\"" +
                                    "onLoad=\"SendDataToPhoneApp()\"> <div id=\"pageWrapper\" style=\"width:100%;\">" + content + "</div></body></html>";

            //wbEnDes.ScriptNotify += wbEnDes_ScriptNotify;
            wbEnDes.NavigateToString(tempHTML);
            wbEnDes.NavigationCompleted += wbEnDes_NavigationCompleted;
        }
        async void wbEnDes_NavigationCompleted(WebView sender, WebViewNavigationCompletedEventArgs args)
        {
            await Task.Delay(1000);
            statusBar.ForegroundColor = Colors.White;
            await statusBar.ProgressIndicator.HideAsync();
            wbEnDes.Visibility = Visibility.Visible;
        }
        /// <summary>
        /// Gets the <see cref="NavigationHelper"/> associated with this <see cref="Page"/>.
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="sender">
        /// The source of the event; typically <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Event data that provides both the navigation parameter passed to
        /// <see cref="Frame.Navigate(Type, object)"/> when this page was initially requested and
        /// a dictionary of state preserved by this page during an earlier
        /// session.  The state will be null the first time a page is visited.</param>
        private async void NavigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
            // TODO: Create an appropriate data model for your problem domain to replace the sample data
            statusBar = StatusBar.GetForCurrentView();
            statusBar.ProgressIndicator.Text = "Đang tải nội dung";
            statusBar.ForegroundColor = Colors.Blue;
            await statusBar.ProgressIndicator.ShowAsync();
            item = await DataSource.GetItemAsync((string)e.NavigationParameter);
            title.Text = item.Title;
            setWeb(item.Content);
            if(item.Fav=="true")
                btnFav.Source = new BitmapImage(new Uri("ms-appx:/Assets/Rating - 02.png"));
            else
                btnFav.Source = new BitmapImage(new Uri("ms-appx:/Assets/Rating - 01.png"));
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="sender">The source of the event; typically <see cref="NavigationHelper"/></param>
        /// <param name="e">Event data that provides an empty dictionary to be populated with
        /// serializable state.</param>
        private void NavigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
            // TODO: Save the unique state of the page here.
        }
        private void showToast(string message)
        {
            var toastXml = ToastNotificationManager.GetTemplateContent(ToastTemplateType.ToastText01);
            var toastElement = toastXml.GetElementsByTagName("text");
            toastElement[0].AppendChild(toastXml.CreateTextNode(message));
            var toast = new ToastNotification(toastXml);
            ToastNotificationManager.CreateToastNotifier().Show(toast);
        }
        #region NavigationHelper registration

        /// <summary>
        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// <para>
        /// Page specific logic should be placed in event handlers for the
        /// <see cref="NavigationHelper.LoadState"/>
        /// and <see cref="NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method
        /// in addition to page state preserved during an earlier session.
        /// </para>
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedFrom(e);
        }

        #endregion

        private async void Image_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (item.Fav == "false")
            {
                item.Fav = "true";
                btnFav.Source = new BitmapImage(new Uri("ms-appx:/Assets/Rating - 02.png"));
                showToast("Đã thêm vào yêu thích");
            }
            else
            {
                item.Fav = "false";
                btnFav.Source = new BitmapImage(new Uri("ms-appx:/Assets/Rating - 01.png"));
                showToast("Bỏ khỏi danh sách yêu thích");
            }
            var content = JsonConvert.SerializeObject(await DataSource.GetGroupsAsync());
            await DataSource.writejson(content);
        }
    }
}