﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Windows.Data.Json;
using Windows.Storage;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;

// The data model defined by this file serves as a representative example of a strongly-typed
// model.  The property names chosen coincide with data bindings in the standard item templates.
//
// Applications may use this model as a starting point and build on it, or discard it entirely and
// replace it with something appropriate to their needs. If using this model, you might improve app 
// responsiveness by initiating the data loading task in the code behind for App.xaml when the app 
// is first launched.

namespace AppPhaCheUniversal.Data
{
    /// <summary>
    /// Generic item data model.
    /// </summary>
    /// 
    public class Item
    {
        public Item()
        { }
        public Item(String uniqueId, String title, String subtitle, String imagePath, String fav, String content)
        {
            this.UniqueId = uniqueId;
            this.Title = title;
            this.Subtitle = subtitle;
            this.ImagePath = imagePath;
            this.Fav = fav;
            this.Content = content;
        }
        public string UniqueId { get; set; }
        public string Title { get; set; }
        public string Subtitle { get; set; }
        public string ImagePath { get; set; }
        public string Fav { get; set; }
        public string Content { get; set; }
        
    }

    /// <summary>
    /// Generic group data model.
    /// </summary>
    /// 
    public class Group
    {
        public Group()
        { }
        public Group(String uniqueId, String title, String subtitle, String imagePath)
        {
            this.UniqueId = uniqueId;
            this.Title = title;
            this.Subtitle = subtitle;
            this.ImagePath = imagePath;
            this.Items = new List<Item>();
        }
        public string UniqueId { get; set; }
        public string Title { get; set; }
        public string Subtitle { get; set; }
        public string ImagePath { get; set; }
        [DataMember]
        public List<Item> Items { get; set; }
        
    }

    /// <summary>
    /// Creates a collection of groups and items with content read from a static json file.
    /// 
    /// SampleDataSource initializes with data read from a static json file included in the 
    /// project.  This provides sample data at both design-time and run-time.
    /// </summary>
    public sealed class DataSource
    {
        private static DataSource _DataSource = new DataSource();

        private List<Group> _groups = new List<Group>();
        public List<Group> Groups
        {
            get { return this._groups; }
        }

        public static async Task<IEnumerable<Group>> GetGroupsAsync()
        {
            await _DataSource.GetDataAsync();
            return _DataSource.Groups;
        }
        public async static Task writejson(string content)
        {
            //MemoryStream stream = new MemoryStream();
            //DataContractJsonSerializer sr = new DataContractJsonSerializer(typeof(List<Group>));
            //sr.WriteObject(stream, _DataSource.Groups);
            //stream.Position = 0;
            //StreamReader reader = new StreamReader(stream);
            //string jsonResult = reader.ReadToEnd();
            //StorageFolder folder = ApplicationData.Current.LocalFolder;
            //StorageFile sampleFile = await folder.CreateFileAsync("congthuc.json", CreationCollisionOption.ReplaceExisting);
            //await FileIO.WriteTextAsync(sampleFile, "{\"Groups\":" + jsonResult + "}");
            Uri dataUri = new Uri("ms-appx:///DataModel/congthuc.json");
            StorageFile file = await StorageFile.GetFileFromApplicationUriAsync(dataUri);
            await FileIO.WriteTextAsync(file, "{\"Groups\":" + content + "}");
        }
        public static async Task<Group> GetGroupAsync(string uniqueId)
        {
            await _DataSource.GetDataAsync();
            // Simple linear search is acceptable for small data sets
            var matches = _DataSource.Groups.Where((group) => group.UniqueId.Equals(uniqueId));
            if (matches.Count() == 1) return matches.First();
            return null;
        }

        public static async Task<Item> GetItemAsync(string uniqueId)
        {
            await _DataSource.GetDataAsync();
            // Simple linear search is acceptable for small data sets
            var matches = _DataSource.Groups.SelectMany(group => group.Items).Where((item) => item.UniqueId.Equals(uniqueId));
            if (matches.Count() == 1) return matches.First();
            return null;
        }
        public static async Task<IEnumerable<Item>> GetItemFavAsync()
        {
            await _DataSource.GetDataAsync();
            return _DataSource.Groups.SelectMany(group => group.Items).Where((item) => item.Fav.Equals("true"));
        }
        private async Task GetDataAsync()
        {
            if (this._groups.Count != 0)
                return;

            Uri dataUri = new Uri("ms-appx:///DataModel/congthuc.json");
            StorageFile file = await StorageFile.GetFileFromApplicationUriAsync(dataUri);
            string jsonText = await FileIO.ReadTextAsync(file); 
            JsonObject jsonObject = JsonObject.Parse(jsonText); 
            JsonArray jsonArray = jsonObject["Groups"].GetArray();

            foreach (JsonValue groupValue in jsonArray)
            {
                JsonObject groupObject = groupValue.GetObject();
                Group group = new Group(groupObject["UniqueId"].GetString(),
                                                            groupObject["Title"].GetString(),
                                                            groupObject["Subtitle"].GetString(),
                                                            groupObject["ImagePath"].GetString());

                foreach (JsonValue itemValue in groupObject["Items"].GetArray())
                {
                    JsonObject itemObject = itemValue.GetObject();
                    group.Items.Add(new Item(itemObject["UniqueId"].GetString(),
                                                       itemObject["Title"].GetString(),
                                                       itemObject["Subtitle"].GetString(),
                                                       itemObject["ImagePath"].GetString(),
                                                       itemObject["Fav"].GetString(),
                                                       itemObject["Content"].GetString()));
                }
                this.Groups.Add(group);
            }
        }
    }
}